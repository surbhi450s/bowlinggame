package com.target.app.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.target.app.constants.GameConstants.MAX_PLAYERS;
import static com.target.app.constants.GameConstants.TOTAL_FRAME;

public class BowlingGame {

	private final int gameId;
	private final int laneNo;
	private int noOfPlayers = MAX_PLAYERS;
	private List<Frame> scoreCard = new ArrayList<>(MAX_PLAYERS);

	public BowlingGame() {
		this.gameId = 0;
		this.laneNo = 0;
	}

	public BowlingGame(int gameId, int laneNo, int noOfPlayers, String[] playersName) {
		super();
		this.gameId = gameId;
		this.laneNo = laneNo;
		if (noOfPlayers < MAX_PLAYERS) {
			this.noOfPlayers = noOfPlayers;
		}
		scoreCard = Stream.generate(Frame::new).limit(MAX_PLAYERS).collect(Collectors.toList());
		for (int i = 0; i < this.noOfPlayers; i++) {
			scoreCard.get(i).setPlayerName(playersName[i]);
		}
	}

	public int getNoOfPlayers() {
		return noOfPlayers;
	}

	public int getGameId() {
		return gameId;
	}

	public int getLaneNo() {
		return laneNo;
	}

	public String[] getPlayersName() {
		String[] players = new String[noOfPlayers];
		for (int i = 0; i < this.noOfPlayers; i++) {

			players[i] = scoreCard.get(i).getPlayerName();
		}
		return players;
	}

	public String getScoreCard(int player) {
		StringBuilder score = new StringBuilder("");
		int currentScore = 0;
		for (int i = 0; i < TOTAL_FRAME; i++) {
			currentScore = scoreCard.get(player).getFrame()[i];
			score.append("\t").append(currentScore);
		}
		score.append("\tTotal Score = ").append(getTotalScore(player));
		return score.toString();
	}

	public int getTotalScore(int player) {
		return scoreCard.get(player).score();
	}

	/**
	 * roll per player
	 */
	public void roll(int pins, int player) {
		if (player > MAX_PLAYERS)
			return;
		scoreCard.get(player).roll(pins);
	}

	public int totalStrike() {
		int totalStrike = 0;
		for (int i = 0; i < noOfPlayers; i++) {
			totalStrike += scoreCard.get(i).currentStrike();
		}
		return totalStrike;
	}

	public int totalSpare() {
		int totalSpare = 0;
		for (int i = 0; i < noOfPlayers; i++) {
			totalSpare += scoreCard.get(i).currentSpare();
		}
		return totalSpare;
	}

	public int missedStrike() {
		int missedStrike = 0;
		for (int i = 0; i < noOfPlayers; i++) {
			missedStrike += scoreCard.get(i).missedStrike();
		}
		return missedStrike;
	}
}