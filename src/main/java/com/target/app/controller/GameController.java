package com.target.app.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import static com.target.app.constants.GameConstants.MAX_PLAYERS;
import static com.target.app.constants.GameConstants.INVALID_PLAYER_DETAILS;
import static com.target.app.constants.GameConstants.JSON_KEY1;
import static com.target.app.constants.GameConstants.JSON_KEY2;

import com.target.app.service.GameService;

@RestController
@RequestMapping(value = "/")
public class GameController {
	@Autowired
	GameService service;

	@GetMapping("/startGame")
	public String startGame(@RequestBody Map<String, String> playerDetails) {
		String response=INVALID_PLAYER_DETAILS;
		
		int noOfPlayers = Integer.parseInt(playerDetails.get(JSON_KEY1));
		String playersName = playerDetails.get(JSON_KEY2);
		String[] players = playersName.split(",");
		int gameId = -1;
		if (noOfPlayers == players.length && noOfPlayers <= MAX_PLAYERS) {
			gameId = service.startGame(noOfPlayers, players);
			response="Game Id= "+gameId;
		}
		return response;
	}

	@GetMapping("/getScoreBoard/{id}")
	public String getScore(@PathVariable int id) {
		return service.getScoreBoard(id);
	}
}