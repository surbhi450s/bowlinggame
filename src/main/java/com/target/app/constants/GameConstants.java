package com.target.app.constants;

public class GameConstants {
	public static final int MAX_PLAYERS=3;
	public static final int TOTAL_FRAME=21;
	public static final int MAX_RANGE=11;
	public static final int PINS=10;
	public static final int FINAL_SET=3;
	public static final String ERROR_MSG="GAME NOT FOUND";
	public static final String INVALID_PLAYER_DETAILS="INVALID PLAYER DETAILS";
	public static final String JSON_KEY1="playerCount";
	public static final String JSON_KEY2="names";
}